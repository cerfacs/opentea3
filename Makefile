init:
	pip install -r requirements.txt

test:
	pytest -v tests --cov=opentea

lint:
	pylint src/opentea

wheel:
	rm -rf build
	rm -rf dist
	python setup.py sdist bdist_wheel

upload_test:
	twine upload --repository-url https://test.pypi.org/legacy/ dist/*

upload:
	twine upload dist/* -u __token__

.PHONY: init test
