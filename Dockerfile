FROM alpine
FROM python:3.6-slim

RUN apt-get update && apt-get install -y \
    git

ADD . /app
WORKDIR /app

RUN pip install -r requirements.txt
# RUN py.test tests --cov=opentea
