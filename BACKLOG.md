
# Backlog

---
*next version*
--- 
- utiliser des menus contextuels dans l'acquisition
- Simplifier les interactions retour 3D (pas de hotkey?)
- Utilisation de save points dans le DO/UNDO pour l'acquisition
- Verification de la validité des données au demarrage (fichier existant)
- Correction comportement des status (process finish combu?)
- MAJ de l'aide 
- Correction du fond gris (mode nuit)
- option des themes au demarrage
- Simplification des objets menus
- passer tous les objets en Ttk
- Chargement du 3D au chargement