#!/usr/bin/env python
"""
OpenTEA scientific GUI library.
Documentation is hosted at: http://cerfacs.fr/opentea
"""

__version__ = "3.9.1"
