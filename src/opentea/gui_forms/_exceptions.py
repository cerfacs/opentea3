class SetException(Exception):
    """Define an exception on the widget setters."""

    pass


class GetException(Exception):
    """Define an exception on the widget getters."""

    pass
