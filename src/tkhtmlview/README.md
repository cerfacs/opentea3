# TKhtmlview

Copy paste of the sources from [tkhtmlview](https://github.com/bauripalash/tkhtmlview), because of the [blocking issue on PIL](https://github.com/bauripalash/tkhtmlview/issues/3).
As soon as the issue is fixed we will move to a normal pip dependency.