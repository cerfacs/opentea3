
## Principle


### The tree

Opentea provides a GUI to set up a tree of degrees of freedom.

- The ROOT is the main window.
- The FIRST nodes are tabs
- Next nodes are containers
- Leaves are the final data : strings, integers, floats or boolean

Two special nodes:
- the MULTIPLE is a list of nodes, sharing the same dof possibilities (e.g. Boundary patches)
- the XOR is a bifurcation between several possibilites.

Sometimes we also use VOID nodes under a XOR, when one possibility require no additional information

### The dependencies

In some situations, we need to add a dependency between nodes - violating the tree structure- . A dependency on node A is added by stating a requirement on B ( `ot_require: B `). Any change on B - a list of strings , normally - will change the configuration of A.

- for A being a MULTIPLE, the items of list A will be named after the content of list of strings B. A is a MULTIPLE DEPENDENT. The user cannot add nor remove items.
- for A being a CHOICE, the choice options of list A will be named after the content of list of strings B. A is a CHOICE DEPENDENT.


## Implementation


### Object Oriented Structure

The current implementation uses -too much- object inheritance.

- `RootWidget` is the uppermost object storing information, used once (irk)
- `_base/OTTreeElement` is the most basic version
    - `node_widgets/OTNodeWidget` is specific to a node
    - `leaf_widgets/_LeafWidget` is specific to a leaf

#### for nodes

- `root_widget/RootTabWidget` is a unique `OTNodeWidget` to gather Tabs holding the forms
- `node_widgets/OTContainerWidget` is a  `OTNodeWidget` without specific data attached.
forms.
- `node_widgets/OTTabWidget` is a  `OTNodeWidget` for the Tabs,  without specific data attached.
- `node_widgets/OTMultipleWidget` is a  `OTNodeWidget` for multiples.
- `node_widgets/OTXorWidget` is a  `OTNodeWidget` for XOrs.

#### for basic leaves

- `leaf_widgets/OTEntry` is the basic `_LeafWidget` entry
- `leaf_widgets/OTNumericEntry` is a `OTEntry` entry with validations on ranges
- `leaf_widgets/OTBoolean` a `_LeafWidget` for boolean choices
- `leaf_widgets/OTFileBrowser` a `_LeafWidget` for file selection

- `leaf_widgets/OTDocu` a `_LeafWidget` for documentation (not saved)
- `leaf_widgets/OTDescription` a `_LeafWidget` for description (not saved)
- `leaf_widgets/OTComment` a string `_LeafWidget` for comments
- `leaf_widgets/OTEmpty` a `_LeafWidget` for empty nodes (not shown, not saved)

#### for leaves choices, it is more swampy:

- `leaf_widgets/_OTChoiceAbstract` an intermediate `_LeafWidget` for choices
- `leaf_widgets/_OTChoiceAbstract` a `_LeafWidget` for choices
- `leaf_widgets/OTChoiceCombo` a `_OTChoiceAbstract` for choices as combo boxes
- `leaf_widgets/OTChoiceRadio` a `_OTChoiceAbstract` for choices as radio choices

- `leaf_widgets/OTChoice` redirects to `_OTChoiceCombo`, `_OTChoiceRadio`  depending on number of items, and to `_OTChoiceDynamic` if it is declared as a `ot_dyn_choice` in schema

- `leaf_widgets/OTChoiceDynamic` a `OTChoiceCombo` for choices that can be dependent of an other variable

#### for leaves lists of data (arrays as leaves), it is even more swampy:

- `leaf_widgets/_OTAbstractList` an intermediate `_LeafWidget` for lists.
- `leaf_widgets/_ListEntry` the subobjet  `_LeafWidget` for lists items.
- `leaf_widgets/DynamicList` an  `_OTAbstractList` for user input. 
- `leaf_widgets/StaticList` an  `_OTAbstractList` for GUI intermediate node input (if `ot_require` or disabled)

#### About leaves

All leaves have a `on_value_change()` triggered by any change on the `Tk.Var`.
This method update the status, then update the widget styling (`_update_leaf_style()`). In the case of entries, it can make additional check(`is_valid()`).
 Inside, `_update_leaf_style()` , one can show an error message (`_update_error_msg()`).

They also have a `validate()` method triggered by the Tab holding this widget.
This one can replace the previous value, and 



### The `.get()` and `.set(data)` methods

`.get()` and `.set(data)` methods are common to all Widgets. The `.get()` returns the data contained. Mean while `.set(data)` change the content of the widget.

A `set(data)` is expected to:
- change the appearance of the widget, by showing the new data
- trigger the `validate()` method, if present, to check if the current data comply to the contraints of the degree of freedom. (Note that `validate()` will also be triggered upon change by the user.)

Both  `.get()` (resp.  `.set()`)  on an node with trigger the `.get()` (resp.  `.set()`) of all its successors. **`.get()` and `.set(data)` are down-recursive**.


### The status

The status is associated to each node, with the following coding:

- `None` Is the initial value of any status
- `1` stands for **valid**
- `0` stands for **unchecked**
- `-1` stands for **invalid**

Most widgets, and above all TabWidgets, should show the current status.
For any node in the tree, its status corresponds to the minimum of all its successors.

Inversely a status change for A means a change for all predecessors of A.
**The status is up-recursive**.

The status behavior has been implemented by adding a setter method. 
This means **the operator `=` triggers more than just an assignation**. 
Indeed `A.status = A.status` will trigger the status update of all predecessors of A.

If A is dependent of B, A is added as a `slave` of B

When the user edit the variable, status can be either -1 or 0. (invalid or unknown).
However if the variable is equal to a previous valid value, it can become valid (1).
It it is a new value, only the `validate()` method can update things.



## Convention on Nested Object

As most nested objects are python dicts, we type them or name name as `dict`.
However the leaves of these objects are strings, floats, integers, and is some case a list appear.

## Concerning tabs updates

Initially the tabs updates were cancelled when data had not changed:

```python
    def set(self, dict_):
        """Get the data of children widgets.

        Input :
        -------
        a dictionary with the value of the childrens
        """
        if dict_ == self.get():
            return
```

However this skipping must not be done. The set triggers the updates events. Cancelling this make dependents widget on other tabs unware of a change. 

For example :  Tab1 have  `aaa` as an integer.  Tab2 have `bbb` as a list with items depending on  `aaa`value
1- User changes `aaa`
2- User hit "Validate" button on Tab1
3- The `set()` is executed on all Tabs
4- Tab1 has changed, so child items of Tab1 are updated
5- User did not change directly values on Tab2. If `set()` of Tab2 is skipped, `bbb` is not updated