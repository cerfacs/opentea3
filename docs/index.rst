.. _header-n0:

.. opentea documentation master file, created by
   sphinx-quickstart on Thu Dec  6 11:21:44 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



Welcome to opentea's documentation!
===================================

.. toctree::
    readme_link
    GUI_BUILDING.md
    ./explanations/developers_notes
    ./api/opentea
    changelog_link

.. _header-n3:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

