# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/).


## [3.9.1] 2025 / 02 / 19

## Fixed

- Correct bad behavior of boolean widgets


## [3.9.0] 2025 / 02 / 19

## Added

- Opentea now supports Graph 2D tabs, with Arnica's Hover items if necessary.

### Changed

- The whole node structure has been changed during ECFD8, especially concerning the status handling.
- The status debugger is now using a colored nobvisual rendering.


## [3.8.0] 2024 / 12 / 05

## Added

- Opentea now supports Acquisition 2D tabs. See calculator example for now


## [3.7.2] 2024 / 12 / 05

## Fixed

- Update the dependency to 3D engine


## [3.7.1] 2024 / 11 / 22

## Changed

- project name changed from OpenTEA to opentea to comply with PEP625

## [3.7.0] 2024 / 11 / 22

## Added

- Support 2D pyplot callback

## Changed

- 3D view for tiny3d-engine is now controlled by a single callback, without I/O
- Cancel the initial skipping of identical updates:  it was needed for dependency btw widgets across tabs


## [3.6.0] 2023 / 07 / 02

## Added

- Verbose mode on the terminal with loguru output

## Changed

- move from package logging to loguru


## [3.5.0] 2023 / 02 / 02

## Added

- Support to `tiny_2d_engine` for enmbedding 2D interactive canvas.

## Changed

- Removal of the aggressive splashscreen when starting a new project
- XOr menus now feature an automatic submenus creation is several prefixes are repeated
- switch to combo boxes is moved from 3+ to  5+ choices.
- radiobuttons are sunken to ease options clustering.



## Fixed

- Display labels in containers corrected
- Spurious status changes fixed
- Simplification of coding 

## [3.4.4] 2022 / 09 / 12

## Fixed

- `validate_light` extended to allow `: {"void": None}` for string validation, because  opentea moved  void container description from `:  None`  to `: {"void": None}`.

## [3.4.3] 2022 / 07 / 13

## Changed
- entries of widget file path are scrollable
## Fixed 
- reload of dependant boundaries
- bug on disabled lists
## [3.4.2] 2022 / 07 / 04

### Changed
- `mem_change` and `mem_check` events removed: goal is to be more explicit
- tree (or graph, for exactness) structure well defined for widgets: it is very easy to fully traverse the tree
    - the tree can be trasversed by assuming all children are dicts
- `status` plays a central role now:
    - controls style of widgets
    - controls update of tab icon
- `status` computation:
    - to increase performance, traverse of full tree is avoided: status computed bottom-up (everytime a leaf status changes, statuses are updated until the top (but only following that path)
- `tree` -> `children`
- `tab` not passed as input to widgets, as it can quickly be retrieved
- `previous_value` comparison for coloring done against saved value
- new objects to handle creation of related objects (e.g. `OTChoice`)
- improved naming regarding public and private objects
- default is set when creating object, instead of using `nob_complete`
- improved `TextConsole`
- more abstraction
- handle of dependents: instead of a global event everytime there's a change in a leaf, leaves now "know" which nodes/leaves depend on them and update them accordingly
- menus are now individual objects: building menus by composition should be trivial (specially useful for connection to external apps, such as `neverd`)
- connection to external apps simplified:
    - `tab_3d` key is still supported, but external apps can now be connect via their own `yaml`


### Fixed
- `smartpacker` non-visible widgets influence



## [3.4.1] 2021 / 10 / 04

### Added
- popup menu with copy, paste and move for `OTMultipleWidget`
- new bindings in `OTMultipleWidget` (move and deselect)
- move up and down buttons in `OTMultipleWidget`
- highlight widgets when they are changed (by changing color)
- add scrollbar to disabled listboxes (`OTListStatic`)
- `MouseScrollFrame`: scrollable frame scrollable via mouse wheel
- add copy-paste popup menu to `OTList` and `OTListStatic`
- add copy-paint binding to `OTMultipleWidget`
- add row number to multiple treeview
- `OTHidden` leaf to make it easier to have nodes that can change value, but are not visible
- add several warning message boxes in `OTMultipleWidget`


### Fixed
- remove double description when `ot_type` in schema
- bindings in `OTMultipleWidget`
- no underscores required as prefix in `OTMultipleItem` names
- `OTMultipleItem` load button behavior
- multiple changes that were not triggering tab icon change
- checkboxes, menus and radiobuttons do not trigger any action if current value is chosen again
- `OTFileBrowser` does not raise error when user cancel path search
- `OTList` behavior: validation of input and error message
- theme selection
- empty titles are not shown in `OTContainer`


### Changed
- `OTMultipleWidget.tree` is now a `dict` (instead of `list`)
- split `OTList` and `OTChoice` in smaller objects for easier mantainability
- replaced several `tk.Label` by `ttk.Label` in order to make the most out of styles
- multiple treeview is now its own object (`MultipleTreeview`)
- name only appears in switchform title in `OTMultipleItem`
- entries with no validation requirements (strings) do not have extra space for status label anymore 



## [3.3.1] 2021 / 05 / 07

### Fixed

- Fix a pb of repeated items for generation of Hybrid GUIs
- Fix the Import Tk issues for usage of process_utils on non graphical environments.
- Add a .yml filter on file dialogs 


## [3.3.0] 2021 / 03 / 09

### Added

- adding `opentea.__version__` attribute though VERSION file
- adding a new project management. A project name is asked if missing
- nobvisual inspection of projects
- recursive fusion of SCHEMA, for composite GUIs

### Changed

- the project is saved at each "Validate/Process"
- cursor switch to waiting mode during "Validate/Process"
- the temp. files like `dataset_to_gui.yml` are now hidden as `.dataset_to*`
- help windows are now rendered using the tkhtmlview package as a top-level window.
- File dialogs store relative paths, not absolute paths


## [3.2.3 ] 2020 / 11 / 09

### Fixed

- the widget comment is no more recursively adding blank lines.


## [3.2.2 ] 2020 / 11 / 09
  
### Changed

- nob_complete can now keep the input data that was not  in the SCHEMA

### Fixed

- some loop holes in validate_light

### Deprecated

- H5proxy is deprecated, should be replaced by hdfdict

## [3.2.1 ] 2020 / 06 / 04

  
### Changed

- Statuses of tabs are recovered when reading a project
- search disabled in consoles by default
- CLI improvements

### Fixed

- spurious dependency on python 3.7 for subprocess, now 3.6 is fine too
- no more deprecated calls to 3D engine
- for documentation : use recomonmark instead of m2r (deprecated)



## [3.2] 2020 / 03 / 13

### Added

- expert dialogs
- dynamic choices
- support of 3D viewer with tiny_3d_engine
  
### Changed

- red output if an error is onprocess
- comments with clear enable/disable mode
- description can tag style tags for itlaliv, bold, small or tiny texts.

### Fixed

 - bug on multiple with depencencies if the list was going to zero


## [3.1.1] 2020 / 01 / 15

### Fixed

- deprecation warning from **h5proxy** removed (consider using hdfdict or H5wrapper instead..)


## [3.1.0] 2019 / 12 / 05

### Added

- **h5proxy** to read quickly in an H5file (consider using hdfdict or H5wrapper instead..)
- **schema2md** convert schema file into HTML tables 


## [3.0.0] 2019 / 03 / 26

### Added

- Tkinter Graphical engine
- noob library
- nob_complete
- nob_validate


### Deprecated

- All Tcl and Python for Version 2 


## [2.3] 2018 / 10 / 17

### Changed

- Documentation using sphinx
- widget info can display icon depending on content

