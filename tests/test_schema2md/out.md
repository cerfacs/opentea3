A representation of a person, company, organization, or place


# fruits

| Key | Types | description | default|
| --- | ----- | ----------- | -------|
|fruits | <span style="color:cyan">array</span> of <span style="color:orange">string</span> |  |  | 

# vegetables

| Key | Types | description | default|
| --- | ----- | ----------- | -------|
|vegetables | <span style="color:cyan">array</span> array | list of items with a structure detailed in the following table |
## List of objects for vegetables
| Key | Types | description | default|
| --- | ----- | ----------- | -------|
|vegetables/veggieName | <span style="color:orange">string</span> | The name of the vegetable. |  | 
|vegetables/veggieLike | <span style="color:purple">boolean</span> | Do I like this vegetable? |  | 
 |  |  | 
